var gulp = require('gulp');
var minifyCss = require('gulp-minify-css');
var csscombLint = require('gulp-csscomb-lint');

gulp.task('minify-css', function() {
	gulp.src('src/*.css')
	.pipe(minifyCss())
	.pipe(gulp.dest('dist'));
});

gulp.task('lint', function() {
	gulp.src(['src/*.css'])
	.pipe(csscombLint());
});

gulp.task('default', ['lint']);