# animation.css - simple CSS animation library

The purpose of this project is to create a common collection of CSS animations that can be used in existing and future projects.

Goals:
* fading inward - center, top, right, bottom, left
* fading outward - center, top, right, bottom, left
* sliding inward - top, right, bottom, left
* zoom inward - top, right, bottom, left
* zoom outward - top, right, bottom, left
* flipping
* noticing/attention animations (ie bounce, wobble, jiggle, etc)

**Note** This project is premature.

# What's been accomplished
* center fade in, fade out
* fading in : top, right, bottom, left
* fading out : top, right, bottom, left
* sliding: top, right, bottom, left
* flipping: 360deg vertically and horizontally


